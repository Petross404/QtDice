#ifndef ANIMATION_H
#define ANIMATION_H

#include "../Dice/dice.hpp"

#include <QtCore/QEvent>
#include <QtCore/QScopedPointer>
#include <QtGui/QMovie>
#include <QtGui/QPixmap>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMessageBox>
#include <QtWidgets/QWidget>
#include <QtWidgets/QGridLayout>

class Animation : public QLabel
{
	Q_OBJECT
public:
	explicit Animation( QWidget* parent, QPixmap* pixmap_ );
	~Animation();

	void replay();
	void setupConnectionsOfAnimation();
	void update( int imageNumber );

private slots:
	void stopLastQMovieFrame( QMovie* movie );

signals:
	void qmovieFrameChanged( QMovie* movie );
	void startedAnimation();
	void finishedAnimation();

protected:
	void contextMenuEvent( QContextMenuEvent* event ) override;

private:
	QScopedPointer<QLabel>		label;
	QScopedPointer<QAction>		reloadAct;
	QScopedPointer<QMovie>		movie;
	QScopedPointer<QPixmap>		pixmap;
	QScopedPointer<QGridLayout>	gridLayout;
	Dice*	pDice;

};
#endif
// kate: indent-mode cstyle; indent-width 8; replace-tabs off; tab-width 8; 
