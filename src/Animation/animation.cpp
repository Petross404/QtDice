#include "Animation/animation.h"

#include <QEvent>
#include <QDebug>
#include <QCloseEvent>

Animation::Animation( QWidget* parent, QPixmap* pixmap_ )
	: QLabel( parent )
	, label( new QLabel )
	, movie( new QMovie( ":resources/images/bluedice.gif" ) )
	, pixmap( new QPixmap( *pixmap_ ) )
	, gridLayout( new QGridLayout )
{
	label->setPixmap( *pixmap.data() );
	label->setAutoFillBackground(true);
	label->setBackgroundRole( QPalette::Base );
	label->setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Maximum );
	label->setScaledContents( true );
	label->setFrameStyle( QFrame::StyledPanel | QFrame::Raised );
	label->setLineWidth( 2 );

	gridLayout->addWidget(label.data(), 0, 0);
	setLayout(gridLayout.data());

	setupConnectionsOfAnimation();
}

Animation::~Animation() = default;

void Animation::replay()
{
	label->setMovie( movie.data() );
	Q_ASSERT_X(movie->isValid(), "Animation::Animation(QWidget* parent)", "Invalid animation");
	if ( Q_UNLIKELY( !movie->isValid() ) )
	{
		QString animationError {"Animation type is not supported! Did you forget to include *.qrc file?"};
		label->setText( tr( animationError.toLocal8Bit().constData() ) );
		QMessageBox::critical( this, tr( "Error" ), tr( animationError.toLocal8Bit().constData() ) );
		exit(-1);
	}
	movie->start();
	if ( movie->state() == QMovie::Running ) { emit startedAnimation(); }
}

void Animation::setupConnectionsOfAnimation()
{
	//Thanks to the guys at this thread :
	//https://forum.qt.io/topic/88197/custom-signal-to-slot-the-slot-requires-more-arguments-than-the-signal-provides
	connect( movie.data(), &QMovie::frameChanged,
	         this, std::bind( &Animation::qmovieFrameChanged, this, movie.data() ) );
 
	connect( this, &Animation::qmovieFrameChanged, this, &Animation::stopLastQMovieFrame );
	connect( movie.data(), &QMovie::finished, this, &Animation::finishedAnimation );
}

// Check if the frame is the last one and stop the dice animation
void Animation::stopLastQMovieFrame( QMovie* movie )
{
	if ( movie->currentFrameNumber() == ( movie->frameCount() - 1 ) )
	{
		movie->stop();
		//Explicity emit finished signal
		//https://bugreports.qt.io/browse/QTBUG-66733
		if ( movie->state() == QMovie::NotRunning ) { emit movie->finished(); }
	}
}

// This function declares image_update(int image_number)
// This function accepts an image_number which is already have been
// generated randomly in Dice class members. Based on image_number it
// selects the proper dice image to show
void Animation::update( int imageNumber )
{
	//Now deal with which image will be loaded based on image_number
	//The whole point of this program is here

	if ( ( imageNumber < 0 ) || ( imageNumber > 6 ) )
	{
		QString errorMsg = "A dice doesn't have this number : "
		                   + ( QString( "%1" ).arg( imageNumber ) );
		QMessageBox::critical( this, tr( "QtDice" ),
		                       tr( errorMsg.toLocal8Bit().constData() ) );
		exit(-1);
	}
	pixmap->load(QString(":/resources/images/dice-%1.png").arg(imageNumber));
	Q_ASSERT_X(!pixmap->isNull(), "Pixmap is null", "void Animation::update(int)");
	label->setPixmap(*pixmap.data());
}

void Animation::contextMenuEvent( QContextMenuEvent* event )
{
	QMenu menu;
	menu.addAction( reloadAct.data() );

	switch ( event->reason() )
	{
		case QContextMenuEvent::Mouse:
			menu.exec( event->globalPos() );
			event->accept();
	}
}
// kate: indent-mode cstyle; indent-width 8; replace-tabs off; tab-width 8; 
