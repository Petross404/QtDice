/* Petros S <petross404@gmail.com>
 *
 * For more information about the license
 * take a look at ../License/GPL-3.txt
 *
 */
#include "dice.hpp"

struct Dice::DiceImplementation
{
	int m_num;
	std::unique_ptr<std::random_device> rd;
	std::unique_ptr<std::mt19937> e2;
	std::unique_ptr<std::uniform_int_distribution<int>> dis;
	DiceImplementation( int num ) : m_num( num ) {}
	DiceImplementation()
		: rd( new std::random_device )
		, e2( new std::mt19937( rd.get()->operator()() ) )
		, dis( new std::uniform_int_distribution<int>( 1, 6 ) )
	{
		m_num = dis.get()->operator()( *e2.get() );
	}
};

Dice* Dice::m_pInstance = nullptr;

Dice* Dice::instance()
{
	if ( !m_pInstance )
	{
		m_pInstance = new Dice;
	}
	return m_pInstance;
}

Dice::Dice( int num ) : d_dice( new DiceImplementation( num ) ) {}

Dice::Dice()
{
	d_dice.reset( new DiceImplementation );
}

Dice::~Dice() = default;

int Dice::getNumber()
{
	if ( ( d_dice->m_num < 1 ) || ( d_dice->m_num > 6 ) )
	{
		std::cerr << "Abort" << std::endl;
		exit( -1 );
	}
	return d_dice->m_num;
}

void Dice::setNumber( int num )
{
	d_dice->m_num = num;
}

void Dice::roll()
{
	d_dice->e2.reset( new std::mt19937( d_dice->rd.get()->operator()() ) );
	d_dice->m_num = d_dice->dis.get()->operator()( *d_dice->e2.get() );
}

// kate: indent-mode cstyle; indent-width 8; replace-tabs off; tab-width 8; 
