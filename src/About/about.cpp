/* Petros S <petross404@gmail.com>
 *
 * For more information about the license
 * take a look at ../License/GPL-3.txt
 *
 * This file defines a messagebox to show up
 * when the user clicks the "About QtDice" action.
 */

#include "about.h"
#include "infotab.h"

About::About( QWidget* parent )
	: QDialog( parent )
	, m_name( new QString )
	, closeBtn( new QPushButton( tr( "&Close" ), this ) )
	, tabWidget( new QTabWidget( this ) )
	, gridLayout( new QGridLayout( this ) )
	, gridTabs( new QGridLayout )
	, gridBtns( new QGridLayout )
	, w_info( new QWidget( this ) )
	, w_contributions( new QWidget( this ) )
	, w_license( new QWidget( this ) )
	, w_thanks( new QWidget( this ) )
{
	gridLayout->addLayout( gridTabs.data(), 0, 0 );
	gridLayout->addLayout( gridBtns.data(), 1, 0 );

	gridTabs->addWidget( tabWidget.data(), 0, 0 );
	gridBtns->addWidget( closeBtn.data(), 0, 5 );
	tabWidget->addTab( w_info.data(), tr( "InfoTab" ) );

	setLayout( gridLayout.data() );
	connect( closeBtn.data(), &QPushButton::clicked,
	         this, &About::close );
}

void About::show()
{
	QDialog::show();
}

#include "moc_about.cpp"
// kate: indent-mode cstyle; indent-width 8; replace-tabs off; tab-width 8; 
