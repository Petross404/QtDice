/* Petros S <petross404@gmail.com>
 *
 * For more information about the license
 * take a look at ../License/GPL-3.txt
 *
 */

#ifndef ABOUT_H
#define ABOUT_H

#include "version.h"

#include <QtCore/QScopedPointer>
#include <QtCore/QString>
#include <QtGui/QImage>
#include <QtGui/QPixmap>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QWidget>

class About : public QDialog
{
	Q_OBJECT

public:
	About( QWidget* parent );

public slots:
	void show();

private:
	QScopedPointer<QString> 	m_name;
	QScopedPointer<QPushButton>	closeBtn;
	QScopedPointer<QTabWidget> 	tabWidget;
	QScopedPointer<QGridLayout>	gridLayout;
	QScopedPointer<QGridLayout>	gridTabs;
	QScopedPointer<QGridLayout>	gridBtns;
	QScopedPointer<QWidget> 	w_info;
	QScopedPointer<QWidget> 	w_contributions;
	QScopedPointer<QWidget> 	w_license;
	QScopedPointer<QWidget> 	w_thanks;

	void setupLayouts();
};
#endif                                                      // ABOUT_H
// kate: indent-mode cstyle; indent-width 8; replace-tabs off; tab-width 8; 
