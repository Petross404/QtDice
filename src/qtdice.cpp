/* Petros S <petross404@gmail.com>
 *
 * For more information about the license
 * take a look at ../License/GPL-3.txt
 *
 */

#include "qtdice.h"
#include "About/about.h"
#include "Settings/configure.h"

QtDice::QtDice( QWidget* parent )
	: QMainWindow( parent ),
	  btnRoll( new QPushButton( tr( "&Roll the dice" ) ) ),
	  btnReset( new QPushButton( tr( "&Reset" ) ) ),
	  btnQuit( new QPushButton( tr( "&Quit" ) ) ),
	  widgetCentral( new QWidget() ),
	  gridLayout( new QGridLayout ),
	  gridLabel( new QGridLayout ),
	  gridStatus( new QGridLayout ),
	  gridWarning( new QGridLayout ),
	  pixmap( new QPixmap( ":/resources/images/dice.png" ) ),
	  labelStatus( new QLabel( tr( "Haven't rolled yet" ) ) ),
	  actionRoll( new QAction( tr( "&Roll the dice" ), this ) ),
	  actionQuit( new QAction( tr( "&Quit" ), this ) ),
	  actionConfigure( new QAction( tr( "&Configure" ), this ) ),
	  actionAboutQt( new QAction( tr( "&About Qt" ), this ) ),
	  actionAbout( new QAction( tr( "&About QtDice" ), this ) ),
	  settings( new QSettings( "QtDice" ) ),
	  quitShortCut( new QShortcut( QKeySequence( Qt::Key_Escape ), this ) ),
	  animation( new Animation( this, pixmap.data() ) ),
	  thread( new QThread( this ) ),
	  pDice( Dice::instance() )
{
	createMenus();

	setWindowTitle( "QtDice" );
	setWindowIcon( QIcon::fromTheme( "roll",
	                                 QIcon( ":/resources/images/dice.ico" ) ) );

	setupWidgets();
	setupLayouts();

	setupConnections();
	setupConnectionsOfAnimation();

	setCentralWidget( widgetCentral.data() );
	centralWidget()->setLayout( gridLayout.data() );
	setMinimumSize( 520, 580 );
	setMaximumSize( 520, 580 );
}

QtDice::~QtDice() = default;

void QtDice::QtDiceConfiguration()
{
	Configure* confWindow = new Configure( this );
	confWindow->show();
}

void QtDice::aboutQtDice()
{
	About* aboutWindow = new About( this );
	aboutWindow->show();
}

//Before showing the image, it plays a small animation of a rolling
//dice for enhanced user experience
void QtDice::animateDice()
{
#ifdef ENABLE_SOUND
	// Decide if the sound will be played...
	if ( isSoundEnabled() )
	{
		roll_sound.play();
		qDebug() << "Play sound";
	}

#endif                                                      // ENABLE_SOUND
	animation->replay();
}

void QtDice::disableWidgets()
{
	//Now that movie is started, set the following...
	btnRoll->setFocus();
	actionRoll->setEnabled( false );
	btnRoll->setEnabled( false );

	//...also set the QLabel that show the status
	labelStatus->setText( tr( "Rolling..." ) );
}

void QtDice::enableWidgets()
{
	animation->show();
	btnRoll->setEnabled( true );
	btnRoll->setFocus();
	actionRoll->setEnabled( true );
	
	//...also set the QLabel that show the status
	labelStatus->setText( tr( "Ready" ) );
}

bool QtDice::isSoundEnabled()
{
	settings->beginGroup( tr( "/sound" ) );
	settings->sync();

	if ( settings->value( "rolling_sound" ).toBool() )
	{
		settings->endGroup();
		return true;
	}
	else
	{
		settings->endGroup();
		return false;
	}
}

// This is the random reload function. After the animation is over, a dice
// is rolled. Look at Dice/dice.cpp for more details.
void QtDice::reload()
{
	animateDice();
	pDice->roll();
}

// This is a reload function. After the animation is over,
// a dice is given a "hardcoded" value. Look at Dice/dice.cpp for more details.
void QtDice::reload( int number )
{
	animateDice();
	pDice->setNumber( number );
}

void QtDice::resetQtDice()
{
	numberOfRolls = 0;
	setupWidgets();
	animation->setPixmap( QPixmap( ":/resources/images/dice.png" ) );
	btnReset->setEnabled( false );
}

void QtDice::createMenus()
{
	menuFile.reset( menuBar()->addMenu( tr( "&File" ) ) );
	menuEdit.reset( menuBar()->addMenu( tr( "&Edit" ) ) );
	menuAbout.reset( menuBar()->addMenu( tr( "&About" ) ) );

	menuFile->addAction( actionRoll.data() );
	actionRoll->setIcon( QIcon( ":/resources/images/dice.ico" ) );
	actionRoll->setShortcuts( {QKeySequence::Refresh,
	                           QKeySequence( Qt::ControlModifier + Qt::Key_R )
	                          } );
	connect( actionRoll.data(), &QAction::triggered, this,
	         static_cast<void ( QtDice::* )( void ) > ( &QtDice::reload ) );

	menuFile->addSeparator();
	menuFile->addAction( actionQuit.data() );

	actionQuit->setIcon( QIcon( ":/resources/images/exit.ico" ) );
	actionQuit->setShortcuts( { QKeySequence( Qt::ControlModifier + Qt::Key_Q ),
	                            QKeySequence::Close
	                          } );

	connect( actionQuit.data(), &QAction::triggered, this, QApplication::quit );

	menuEdit->addAction( actionConfigure.data() );
	connect( actionConfigure.data(), &QAction::triggered, this, &QtDice::QtDiceConfiguration );
	actionConfigure->setShortcuts( { QKeySequence( Qt::ControlModifier + Qt::Key_E ),
	                                 QKeySequence::Preferences
	                               } );

	actionConfigure->setIcon( QIcon::fromTheme( "settings-configure" ) );

	menuAbout->addAction( actionAbout.data() );
	actionAbout->setIcon( QIcon::fromTheme( "help-about", QIcon( ":/resources/images/dice.ico" ) ) );
	connect( actionAbout.data(), &QAction::triggered, this, &QtDice::aboutQtDice );

	menuAbout->addAction( actionAboutQt.data() );
	actionAboutQt->setIcon( QIcon( ":/resources/images/Qt_logo_2016.svg.ico" ) );
	actionAboutQt->setShortcut( QKeySequence( Qt::ControlModifier + Qt::Key_B ) );
	connect( actionAboutQt.data(), &QAction::triggered, this, &QApplication::aboutQt );
}

void QtDice::setupConnections()
{
	//Connect buttons to functions
	connect( btnRoll.data(), &QPushButton::clicked, this,
	         static_cast<void ( QtDice::* )( void ) > ( &QtDice::reload ) );
	connect( btnQuit.data(), &QPushButton::clicked, this, &QApplication::quit );
	connect( btnReset.data(), &QPushButton::clicked, this, &QtDice::resetQtDice );
}

void QtDice::setupConnectionsOfAnimation()
{
	// When animation starts, disable the buttons etc...
	connect( animation.data(), &Animation::startedAnimation, this, &QtDice::disableWidgets );

	// When the movie is finished, re-enable buttons etc
	connect( animation.data(), &Animation::finishedAnimation, this, &QtDice::enableWidgets );
	auto f = [this](){ animation->update( pDice->getNumber()); };
	connect( animation.data(), &Animation::finishedAnimation,
		animation.data(),
		[this]()
		{
			animation->update( pDice->getNumber() );
		}
	);
}

void QtDice::setupLayouts()
{
	gridLayout->addLayout( gridLabel.data(), 0, 0 );
	gridLayout->addLayout( gridStatus.data(), 1, 0 );
	gridLayout->addLayout( gridWarning.data(), 2, 0 );

	gridLabel->addWidget( animation.data(), 0, 0 );

	gridStatus->addWidget( labelStatus.data(), 0, 0 );
	gridStatus->addWidget( btnRoll.data(), 0, 2 );

	gridWarning->addWidget( btnReset.data(), 0, 0 );
	gridWarning->addWidget( btnQuit.data(), 0, 1 );
}

void QtDice::setupWidgets()
{
	btnRoll->setIcon( QIcon::fromTheme( "roll",
	                                    QIcon( ":/resources/images/dice.ico" ) ) );
	btnQuit->setIcon( QIcon::fromTheme( "application-exit",
	                                    QIcon( ":/resources/images/exit.ico" ) ) );

	btnReset->setEnabled( false );
}

#include "moc_qtdice.cpp"
// kate: indent-mode cstyle; indent-width 8; replace-tabs off; tab-width 8; 
