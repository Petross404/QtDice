/* Petros S <petross404@gmail.com>
 *
 * For more information about the license
 * take a look at ../License/GPL-3.txt
 *
 */

#include "qtdice.h"
#include <QApplication>
#include <QCommandLineParser>
#include <QCommandLineOption>

int main( int argc, char* argv[] )
{
	QApplication::setAttribute( Qt::AA_UseHighDpiPixmaps );
	QApplication app( argc, argv );

	QCommandLineParser parser;
	QCommandLineOption info( QStringList() << "i" << "info",
	                               QApplication::translate( "Main",
	                                               "Print informations" ));

	parser.addOption( info );
	parser.process( app );

	QScopedPointer<QtDice> diceWindow( new QtDice() );
	diceWindow->show();

	return app.exec();
}
// kate: indent-mode cstyle; indent-width 8; replace-tabs off; tab-width 8; 
